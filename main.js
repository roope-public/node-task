const express = require("express");
const app = express();
const PORT = 3000;
// build small REST API with Express
console.log("Server-side program starting...");

// baseurl is where the application or REST API starts
// and the endpoints are inside that REST API. They might have different paths
// baseurl: http://localhost:3000/

// app.get is an endpoint in our program
// Endpoint: http://localhost:3000/
app.get('/', (req, res) => {
    res.send('Hello world');
})

/**
 * This arrow function adds two numbers together
 * @param {Number} a first parameter
 * @param {Number} b second parameter
 * @returns {Number} the sum of a and b
 */
const add = (a,b) => {
    const sum = Number(a) + Number(b);
    return sum
}

// Adding endpoint: http://localhost:3000/add?a=value&b=value
app.get('/add', (req, res) =>{
    console.log(req.query);
    const sum = add(req.query.a, req.query.b);
    res.send(sum.toString());
})

app.listen(PORT, ()=> console.log(
    `Server listening http://localhost:${PORT}`
))